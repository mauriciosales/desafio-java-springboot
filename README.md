<h1 align="center">
   <a href="">Productms-Challenge</a>
</h1>
<p align="center">A challenge app written in java springboot for technical interview</p>

![Logo Compasso](https://compasso.com.br/wp-content/uploads/2020/07/LogoCompasso-Negativo.png)



Content Table
==========
<!--ts-->
*   [About](#About)
*   [Features](#Features)
*   [Demo application](#Demo-application)
*   [Getting started](#Getting-started)
    *   [Start](#Start)
    *   [Requirements](#Requirements)
*   [Tecnologies](#Tecnologies)
<!--te-->

### About

    This is an application where you can make simple budgets and relate them with companines.
    With our application, you can easily organize budgets and produce reports.

### Features
*   [x]   Creating a product
*   [X]   Product list
*   [X]   Deletion of a product
*   [x]   Updating a product
*   [x]   Searching for a product by ID
*   [x]   List of filtered products

### Demo application

show some gif

### Getting started


#### Start

To get for development, we need to clone the project repo using some terminal like git bash and then use the following code:

-   `cd path of your preference`
-   `git clone https://bitbucket.org/mauriciosales/desafio-java-springboot.git`
-   `cd desafio-java-springboot`

To get a jar file, run on maven the following command 
 
- `mvnw clean package` 
- `get the jar file on target path of the project`


#### Requirements
*   [JDK 11: Required to run a java project](https://www.oracle.com/br/java/technologies/javase-jdk11-downloads.html)

*   [Maven 3.5.3: Required to do a build Java project](http://mirror.nbtelecom.com.br/apache/maven/maven-3/3.5.3/binaries/apache-maven-3.5.3-bin.zip)

After that, you can import project to Eclipse, Intellij or Vscode
*   [Eclipse: for development the project](http://www.eclipse.org/downloads/packages/eclipse-ide-java-ee-developers/oxygen3a)
*   [Intellij for development the project](https://www.jetbrains.com/)
*   [Vscode for development the project](https://code.visualstudio.com/)

### Tecnologies

The following tools were used on construction project:

*   [Java 8](https://www.java.com/pt-BR/download/manual.jsp)
*   [Spring](https://spring.io/)
*   [Spring Boot](https://spring.io/projects/spring-boot)
*   [Lombok](https://projectlombok.org/)
*   [JUnit 5](https://junit.org/junit5/)
*   [Swagger](https://swagger.io/)



#   Stories based  on BDD - Behavior Driven Development

##  Product Story (persist product)
> As **User or Admin**, i want to be able to persist some product, so that i may make sales.
>
>**Scenario 1**: Invalid Company
>
> **Given** the user is on window product<br>
> **when** fill  name, description , price with null<br>
> **then**  should return Business exception when products is null.
>
> **Scenario 2**: Valid product
>
> **Given** the user or admin is on window product,<br>
> **when** fill with name<br>
> **And** fill with description<br>
> **And** fill with price<br>
> **then** should return a persisted product.
>
> Measurement of success:

##  Product Story (Find product)
> As **User or Admin**, i want to be able to find some product, so that i may make sales for that product.
>
>**Scenario 1**: Don´t find Product
>
> **Given** the user is on window find prodcut <br>
> **when** fill with product id<br>
> **then**  should return a exception when respective product can´t be founded.
>
> **Scenario 2**: Find Product
>
> **Given** the user or admin is on window product<br>
> **when** fill with id<br>
> **then** should return a product.
> 



> ##  Product Story (update product)
> As **User or Admin**, i want to be able to update some product, so that i may make sales for that product.
>
>**Scenario 1**: Invalid Product
>
> **Given** the user is on window product<br>
> **when** fill with id product with null or null object product<br>
> **then**  should return not founded when product is null.
>
> **Scenario 2**: Valid updated Company
>
> **Given** the user or admin is on window product<br>
> **when** fill with name <br>
> **Or** fill with description<br>
> **Or** fill with price <br>
> **then** should return a updated company.



##  Product Story (delete product)
> As **User or Admin**, i want to be able to delete some product, so that i may make sales for that product.
>
>**Scenario 1**: Invalid Product
>
> **Given** the user is on window Product<br>
> **when** fill with id product with null<br>
> **then**  should return not found when product is null.
>
> **Scenario 2**: Valid deletion Product
>
> **Given** the user or admin is on window product<br>
> **when** fill with  id<br>
> **then** should return a deleted product.
>

# Catálogo de produtos

Sua tarefa é implementar um catálogo de produtos com Java e Spring Boot.

## product-ms

Neste microserviço deve ser possível criar, alterar, visualizar e excluir um determinado produto, além de visualizar a lista de produtos atuais disponíveis. Também deve ser possível realizar a busca de produtos filtrando por *name, description e price*.

### Formato

O formato esperado de um produto é o seguinte:

```javascript
  {
    "id": "string",
    "name": "string",
    "description": "string",
    "price": 59.99
  }
```
Durante a criação e alteração, os campos *name, description e price* são obrigatórios. Em relação ao campo *price* o valor deve ser positivo.

### Endpoints

Devem ser disponibilizados os seguintes endpoints para operação do catálogo de produtos:


| Verbo HTTP  |  Resource path    |           Descrição           |
|-------------|:-----------------:|------------------------------:|
| POST        |  /products        |   Criação de um produto       |
| PUT         |  /products/{id}   |   Atualização de um produto   |
| GET         |  /products/{id}   |   Busca de um produto por ID  |
| GET         |  /products        |   Lista de produtos           |
| GET         |  /products/search |   Lista de produtos filtrados |
| DELETE      |  /products/{id}   |   Deleção de um produto       |

#### POST /products

Esse endpoint deve criar um novo produto na base de dados, ao receber o JSON do produto o mesmo deverá ser validado em acordo com as regras da seção **Formato**, e, caso esteja válido, persistido na base de dados e retornado com o *id* gerado e HTTP 201.

Entrada:
```javascript
  {
    "name": "nome",
    "description": "descrição",
    "price": <preco>
  }
```

Retorno:
```javascript
  {
    "id": "id gerado",
    "name": "nome",
    "description": "descrição",
    "price": <preco>
  }
```

Em caso de algum erro de validação, a API deve retornar um HTTP 400 indicando uma requisição inválida. O JSON retornado nesse caso deve seguir o seguinte formato:

```javascript
  {
    "status_code": integer,
    "message": "string"
  }
```
No campo *status_code* deve vir o código HTTP do erro de validação (400 Bad Request). No campo *message* deverá vir uma mensagem genérica indicando o erro ocorrido.

#### PUT /products/\{id\}

Esse endpoint deve atualizar um produto baseado no {id} passado via path param. Antes de alterar, deve ser consultada a base de dados pelo *id*, se o produto NÃO for localizado então devolver um HTTP 404 ao cliente. Se localizar o produto, então os campos *name, description e price* devem ser atualizados conforme recebidos no body da requisição.

Entrada:
```javascript
  {
    "name": "nome",
    "description": "descrição",
    "price": <preco>
  }
```

Retorno:
```javascript
  {
    "id": "id atualizado",
    "name": "nome",
    "description": "descrição",
    "price": <preco>
  }
```

Importante que antes da atualização as mesmas regras de validação do POST /products devem ser executadas para garantir consistência, e, se ocorrer erro retornar no mesmo formato:

```javascript
  {
    "status_code": integer,
    "message": "string"
  }
```

#### GET /products/\{id\}

Esse endpoint deve retornar o product localizado na base de dados com um HTTP 200. Em caso de não localização do produto, a API deve retornar um HTTP 404 indicando que o recurso não foi localizado, não há necessidade de retornar um JSON (response body) nesse caso.

Retorno:
```javascript
  {
    "id": "id buscado",
    "name": "nome",
    "description": "descrição",
    "price": <preco>
  }
```

#### GET /products

Nesse endpoint a API deve retornar a lista atual de todos os produtos com HTTP 200. Se não existir produtos, retornar uma lista vazia.

Retorno com produtos:
```javascript
[
  {
    "id": "id produto 1",
    "name": "nome",
    "description": "descrição",
    "price": <preco>
  },
  {
    "id": "id produto 2",
    "name": "nome",
    "description": "descrição",
    "price": <preco>
  }
]
```

Retorno vazio:
```javascript
[]
```

#### GET /products/search

Nesse endpoint a API deve retornar a lista atual de todos os produtos filtrados de acordo com query parameters passados na URL.

Os query parameters aceitos serão: q, min_price e max_price.

**Importante: nenhum deles deverá ser obrigatório na requisição**

Onde:

| Query param |  Ação de filtro     
|-------------|:---------------------------------------------------------------:|
| q           |  deverá bater o valor contra os campos *name* e *description*   |
| min_price   | deverá bater o valor ">=" contra o campo *price*                |
| max_price   | deverá bater o valor "<=" contra o campo *price*                |

**Exemplo: /products/search?min_price=10.5&max_price=50&q=superget**

Retorno com produtos filtrados/buscados:
```javascript
[
  {
    "id": "id produto 1",
    "name": "nome",
    "description": "descrição",
    "price": <preco>
  },
  {
    "id": "id produto 2",
    "name": "nome",
    "description": "descrição",
    "price": <preco>
  }
]
```

Retorno vazio:
```javascript
[]
```

#### DELETE /products/\{id\}

Esse endpoint deve deletar um registro de produto na base de dados. Caso encontre o produto filtrando pelo *id* então deve deletar e retornar um HTTP 200. Se o *id* passado não foi localizado deve retornar um HTTP 404

## Validação

A validação dos endpoints e sua correta funcionalidade será através de script automatizado. Logo, é importante que você defina a porta do serviço como sendo 9999, ficando a base url então: http://localhost:9999

Também ocorrerá avaliação técnica do código-fonte produzido, bem como eventual análise estática do mesmo.

## Entrega do código

Você é responsável por entregar o código da forma como achar mais adequado, bem como eventuais documentações necessárias para a execução do seu microserviço.

### Bom desafio \m/