package com.uol.desafiojavaspringboot.model.repository;

import com.uol.desafiojavaspringboot.model.Product;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Example;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;


import java.math.BigDecimal;
import java.util.UUID;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")

@DataJpaTest
public class ProductRepositoryTest {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    @Test
    @DisplayName("should persist a product")
    public void returnAPersistProductTest(){
        Product product=Product.builder()
                .description("USB M160 Preto")
                .name("Mouse HP Gamer")
                .price(new BigDecimal("23.45"))
                .build();

        Product savedProduct= productRepository.save(product);

        Assertions.assertThat(savedProduct.getId()).isNotNull();
        Assertions.assertThat(savedProduct.getPrice()).isPositive();

    }

    @Test
    @DisplayName("should return a product")
    public void returnProductByIdTest(){

        Product product=Product.builder()
                .description("USB M160 Preto")
                .name("Mouse HP Gamer")
                .price(new BigDecimal("23.45"))
                .build();
        testEntityManager.persist(product);

        //Product foundedProduct= productRepository.findById()
        boolean existedProduct= productRepository.exists(Example.of(product));

    }

    @Test
    @DisplayName("should return a empty product")
    public void returnNotFoundProductTest(){
        Product product=Product.builder()
                .description("USB M160 Preto")
                .name("Mouse HP Gamer")
                .price(new BigDecimal("23.45"))
                .build();
        boolean existedProduct= productRepository.exists(Example.of(product));

        Assertions.assertThat(existedProduct).isFalse();
    }


}
