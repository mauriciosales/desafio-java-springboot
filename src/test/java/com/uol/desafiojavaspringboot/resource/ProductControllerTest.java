package com.uol.desafiojavaspringboot.resource;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uol.desafiojavaspringboot.model.Product;

import com.uol.desafiojavaspringboot.model.ProductFilter;
import com.uol.desafiojavaspringboot.resource.dto.ProductDTO;
import com.uol.desafiojavaspringboot.service.ProductService;

import org.hamcrest.Matchers;
import org.hibernate.validator.constraints.pl.PESEL;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@WebMvcTest(controllers = ProductController.class)
@AutoConfigureMockMvc

public class ProductControllerTest {

    @Autowired
    MockMvc mvc;

    @MockBean
    private ProductService productService;

     private static final String PRODUCT_API="/products";

    @Test
    @DisplayName("should return product created")
    public void returnProductTest() throws Exception {
        ProductDTO productDTO=ProductDTO.builder().description("USB M160 Preto").name("Mouse HP Gamer").price(new BigDecimal("23.45")).build();

        Product product=Product.builder().description("USB M160 Preto").name("Mouse HP Gamer").price(new BigDecimal("23.45")).build();
        String json=new ObjectMapper().writeValueAsString(productDTO);
        BDDMockito.given(productService.create(Mockito.any(Product.class))).willReturn(product);


        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(PRODUCT_API)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(json);

        mvc.perform(request)
                .andExpect(status().isCreated())
                .andExpect(jsonPath("name").value("Mouse HP Gamer"))
                .andExpect(jsonPath("description").value("USB M160 Preto"))
        ;

    }

    @Test
    @DisplayName("should return bad request when try to persist a empty name field product")
    public void returnBadRequestWithEmptyNameTest() throws Exception {
        ProductDTO productDTO=ProductDTO.builder().description("USB M160 Preto").name(null).price(new BigDecimal("23.45")).build();

        String json= new ObjectMapper().writeValueAsString(productDTO);


        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(PRODUCT_API)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc.perform(request)
                .andExpect(status().isBadRequest());

    }

    @Test
    @DisplayName("should return bad request when try to persist a empty description field product")
    public void returnBadRequestWithEmptyDescriptionTest() throws Exception {
        ProductDTO productDTO=ProductDTO.builder().name("Mouse HP Gamer").description("").price(new BigDecimal("23.45")).build();

        String json= new ObjectMapper().writeValueAsString(productDTO);


        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(PRODUCT_API)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc.perform(request)
                .andExpect(status().isBadRequest())

                //.andExpect(jsonPath("arguments", Matchers.hasSize(1)))

        ;

    }

    @Test
    @DisplayName("should return bad request when try to persist a empty price field product")
    public void returnBadRequestWithNullPriceTest() throws Exception {
        ProductDTO productDTO=ProductDTO.builder().description("USB M160 Preto").name("Mouse HP Gamer").price(null).build();

        String json= new ObjectMapper().writeValueAsString(productDTO);


        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(PRODUCT_API)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc.perform(request)
                .andExpect(status().isBadRequest());

    }


    @Test
    @DisplayName("should return bad request when try to persist a empty price less than zero field product")
    public void returnBadRequestWithPriceLessThanZeroTest() throws Exception {
        ProductDTO productDTO=ProductDTO.builder().description("USB M160 Preto").name("Mouse HP Gamer").price(new BigDecimal(-1)).build();

        String json= new ObjectMapper().writeValueAsString(productDTO);


        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(PRODUCT_API)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc.perform(request)
                .andExpect(status().isBadRequest());

    }

    @Test
    @DisplayName("should return a list of products")
    public void returnListProductsTest() throws Exception {

        List<Product> productList= Arrays.asList(newProduct());
        BDDMockito.given(productService.findAll()).willReturn(productList);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.get(PRODUCT_API)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON);

        mvc.perform(request)
                .andExpect(status().isOk())
         .andExpect(jsonPath("$", Matchers.hasSize(1)))

        ;
    }

    @Test
    @DisplayName("should return a product by id field")
    public void returnProductByIdTest() throws Exception {
        UUID id=UUID.randomUUID();
        Product product=newProduct();
        product.setId(id);
        BDDMockito.given(productService.findByUuid(Mockito.any(UUID.class))).willReturn(Optional.of(product));

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get(PRODUCT_API.concat("/" + id))
                .accept(MediaType.APPLICATION_JSON);

        mvc.perform(request).andExpect(status().isOk());


    }

    @Test
    @DisplayName("should return not found status when try to find product by id")
    public void returnNotFoundWhenFindByIdTest() throws Exception {
        String id=UUID.randomUUID().toString();

        BDDMockito.given(productService.findByUuid(Mockito.any(UUID.class))).willReturn(Optional.empty());

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get(PRODUCT_API.concat("/" + id))
                .accept(MediaType.APPLICATION_JSON);

        mvc.perform(request).andExpect(jsonPath("status_code").value("404"));
    }

    @Test
    @DisplayName("should return a updated product")
    public void returnUpdatedProductTest() throws Exception {
        UUID id=UUID.randomUUID();
        ProductDTO productDTO=newProductDTO();
        productDTO.setId(id.toString());
        Product product=newProduct();
        product.setId(id);
        String json=new ObjectMapper().writeValueAsString(productDTO);


        BDDMockito.given(productService.update(Mockito.any(Product.class))).willReturn(product);


        BDDMockito.given(productService.findByUuid(Mockito.any(UUID.class))).willReturn(Optional.of(product));

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .put(PRODUCT_API.concat("/" + id))
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc.perform(request).andExpect(status().isOk());
    }

    @Test
    @DisplayName("should return a not founded product when try to update")
    public void returnNotFoundProductTest() throws Exception {
        UUID id=UUID.randomUUID();
        BDDMockito.given(productService.findByUuid(Mockito.any(UUID.class))).willReturn(Optional.empty());

        ProductDTO productDTO=newProductDTO();
        productDTO.setId(id.toString());
        String json=new ObjectMapper().writeValueAsString(productDTO);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .put(PRODUCT_API.concat("/" + id))
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc.perform(request).andExpect(jsonPath("status_code").value("404"));
    }



    @Test
    @DisplayName("should delete a product")
    public void deleteProductTest() throws Exception {
        UUID id =UUID.randomUUID();
        Product product=newProduct();
        product.setId(id);

        BDDMockito.given(productService.findByUuid(Mockito.any(UUID.class))).willReturn(Optional.of(product));

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.delete(PRODUCT_API.concat("/" + id))
                .accept(MediaType.APPLICATION_JSON);

        mvc.perform(request).andExpect(status().isNoContent());
    }

    @Test
    @DisplayName("should throw an excepition when try to delete a product that doesn´t exist")
    public void returnNotFoundDeleteProductTest() throws Exception {
        UUID id=UUID.randomUUID();

        BDDMockito.given(productService.findByUuid(Mockito.any(UUID.class))).willReturn(Optional.empty());

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.delete(PRODUCT_API.concat("/" + id));

        mvc.perform(request).andExpect(jsonPath("status_code").value("404"));

    }

    @Test
    @DisplayName("should return a list of products filter by Min_price, Max_price or(and) q parameter")
    public void returnListProductFilter() throws Exception {
        String min_price= new BigDecimal("23.45").toString();
        String max_price= new BigDecimal("34.45").toString();
        String q= newProduct().getDescription();

        Product product=newProduct();
        List<Product> productList= Arrays.asList(product);


        BDDMockito.given(productService.filter(Mockito.any(ProductFilter.class))).willReturn(productList);

        String queryString= String.format("?min_price=%s&max_price=%s&q=%s",min_price,max_price,q) ;

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get(PRODUCT_API.concat("/search" +queryString))
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                ;
        mvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.*",Matchers.hasSize(1)))
                .andExpect(jsonPath("$",Matchers.hasSize(1)));
                ;
    }

    @Test
    @DisplayName("should return a empty product list")
    public void returnEmptyProductList() throws Exception {
        List<Product> productLidst=Arrays.asList();
        String min_price= new BigDecimal("23.45").toString();
        String max_price= new BigDecimal("34.45").toString();
        String q= newProduct().getDescription();

        BDDMockito.given(productService.filter(Mockito.any(ProductFilter.class))).willReturn(productLidst);

        String queryString=String.format("?min_price=%s&max_price=%s&q=%s",min_price,max_price,q);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.get(PRODUCT_API.concat("/search" + queryString))
                .accept(MediaType.APPLICATION_JSON);
        mvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$",Matchers.hasSize(0)))
        ;
    }



    public static Product newProduct(){

       return Product.builder()
                .name("Mouse HP Gamer")
                .description("USB M160 Preto")
                .price(new BigDecimal("23.45"))
                .build();

    }

    public static ProductDTO newProductDTO(){

        return ProductDTO.builder()
                .name("Mouse HP Gamer")
                .description("USB M160 Preto")
                .price(new BigDecimal("23.45"))
                .build();
    }

}
