package com.uol.desafiojavaspringboot.service;

import com.uol.desafiojavaspringboot.model.Product;

import com.uol.desafiojavaspringboot.model.ProductFilter;
import com.uol.desafiojavaspringboot.model.repository.*;
import com.uol.desafiojavaspringboot.service.impl.ProductServiceImpl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatcher;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.ActiveProfiles;

import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
public class ProductServiceTest {


    @MockBean
    private ProductRepository repository;

    private ProductService productService;

    @BeforeEach
    void setUp(){
        this.productService=new ProductServiceImpl(repository);
    }


    @Test
    @DisplayName("should return a created product")
    public void returnCreatedProductTest(){
        Product product= Product.builder().id(UUID.randomUUID()).description("USB M160 Preto").name("Mouse HP Gamer").price(new BigDecimal(23.45)).build();


        Mockito.when(repository.save(Mockito.any(Product.class))).thenReturn(product);

        Product savedProduct=productService.create(product);

        assertThat(savedProduct.getId()).isNotNull();
        assertThat(savedProduct.getName()).isEqualTo("Mouse HP Gamer");
        assertThat(savedProduct.getDescription()).isEqualTo("USB M160 Preto");
        assertThat(savedProduct.getPrice()).isEqualTo(new BigDecimal(23.45));


    }

    @Test
    @DisplayName("should return a product by id")
    public void returnProductByIdTest(){

        UUID id =UUID.randomUUID();
        Product product=Product.builder()
                .id(id)
                .description("USB M160 Preto")
                .name("Mouse HP Gamer")
                .price(new BigDecimal(23.45))
                .build();

        Mockito.when(repository.findById(id)).thenReturn(Optional.of(product));

        Optional<Product> savedProduct= productService.findByUuid(id);

        assertThat(savedProduct).isNotNull();
        assertThat(savedProduct.get().getId()).isEqualTo(id);
    }

    @Test
    @DisplayName("should return a not found when try to find product by id")
    public void returnNotFoundProductTest(){
        UUID id =UUID.randomUUID();

        Mockito.when(repository.findById(id)).thenReturn(Optional.empty());

        Optional<Product> savedProduct= productService.findByUuid(id);

        assertThat(savedProduct).isEmpty();


    }
     @Test
    @DisplayName("should return a updated product")
    public void returnUpdatedProductTest(){
        UUID id= UUID.randomUUID();
        Product product= Product.builder()
                .id(id)
                .description("USB M160 Preto")
                .name("Mouse HP Gamer")
                .price(new BigDecimal(23.45))
                .build();
        Mockito.when(repository.findById(Mockito.any(UUID.class))).thenReturn(Optional.of(product));

        Mockito.when(repository.save(Mockito.any(Product.class))).thenReturn(product);

        Product savedProduct= productService.update(product) ;

        assertThat(savedProduct).isNotNull();
        assertThat(savedProduct.getId()).isNotNull();

     }



    @Test
    @DisplayName("should delete a product")
    public void deleteProductTest(){
        UUID id=UUID.randomUUID();
        Product product= Product.builder()
                .id(id)
                .description("USB M160 Preto")
                .name("Mouse HP Gamer")
                .price(new BigDecimal(23.45))
                .build();

        Mockito.when(repository.findById(Mockito.any(UUID.class))).thenReturn(Optional.of(product));

        Assertions.assertDoesNotThrow(()->productService.delete(product));

        Mockito.verify(repository,Mockito.times(1)).delete(product);
    }

    @Test
    @DisplayName("should return not found exception when try to delete product")
    public void deleteNotExistProductTest(){

        UUID id=UUID.randomUUID();
        Product product= new Product();

        Mockito.when(repository.findById(Mockito.any(UUID.class))).thenReturn(Optional.empty());



        Mockito.verify(repository, Mockito.never()).delete(product);
    }


    @Test
    @DisplayName("should return product list filter by name")
    public void returnProductListFilterTest(){

        String q= "Mouse";
        ProductFilter productFilter= ProductFilter.builder().q(q).build();
        UUID id=UUID.randomUUID();
        Product product= Product.builder()
                .id(id)
                .description("USB M160 Preto")
                .name("Mouse HP Gamer")
                .price(new BigDecimal(23.45))
                .build();
        Product product1=Product.builder()
                .id(UUID.randomUUID())
                .description("USB M160 Preto")
                .name("Mouse2 HP Gamer")
                .price(new BigDecimal(23.45))
                .build();
        ;
        List<Product> productList= Arrays.asList(product1,product);


        Mockito.when(repository.findAll(Specification.where(Mockito.any(Specification.class)))).thenReturn(productList);

        List<Product> returnedProductList= productService.filter(productFilter);

        org.assertj.core.api.Assertions.assertThat(returnedProductList.size()).isEqualTo(2);

    }
    @Test
    @DisplayName("should return product list filter by name")
    public void returnEmptyProductListFilterTest(){

        String q= "Mouse";
        ProductFilter productFilter= ProductFilter.builder().q(q).build();
        UUID id=UUID.randomUUID();

        List<Product> productList= Arrays.asList();


        Mockito.when(repository.findAll(Specification.where(Mockito.any(Specification.class)))).thenReturn(productList);

        List<Product> returnedProductList= productService.filter(productFilter);

        org.assertj.core.api.Assertions.assertThat(returnedProductList.size()).isEqualTo(0);

    }




}
