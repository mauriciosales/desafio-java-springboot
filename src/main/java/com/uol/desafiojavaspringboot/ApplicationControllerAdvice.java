package com.uol.desafiojavaspringboot;

import com.uol.desafiojavaspringboot.resource.exception.ApiErrors;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@RestControllerAdvice
public class ApplicationControllerAdvice {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiErrors handleValidationErros(MethodArgumentNotValidException exception) {

        BindingResult bindingResult = exception.getBindingResult();
        Optional<String> errorMessages = bindingResult.getAllErrors()
                .stream()
                .map(objectError -> objectError.getDefaultMessage())
                .findFirst();

                int status_code=HttpStatus.BAD_REQUEST.value();
        return new ApiErrors(status_code, errorMessages.get());
    }

    @ExceptionHandler(ResponseStatusException.class)
    public ApiErrors handleResponseStatusException(ResponseStatusException exception) {
        String message = exception.getMessage();

        int codigo = exception.getRawStatusCode();

        return new ApiErrors( codigo,message);
    }
}