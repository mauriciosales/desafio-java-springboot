package com.uol.desafiojavaspringboot.service.impl;

import com.uol.desafiojavaspringboot.model.Product;
import com.uol.desafiojavaspringboot.model.ProductFilter;
import com.uol.desafiojavaspringboot.model.repository.*;
import com.uol.desafiojavaspringboot.service.ProductService;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ProductServiceImpl implements ProductService {
    private  ProductRepository productRepository;

    public ProductServiceImpl(ProductRepository repository) {
        this.productRepository=repository;
    }

    @Override
    public Product create(Product product) {
        return productRepository.save(product);
    }

    @Override
    public List<Product> findAll() {
        return productRepository.findAll();
    }


    @Override
    public Optional<Product> findByUuid(UUID id) {
        //System.out.println(id.toString());
        return productRepository.findById(id);
    }

    @Override
    public Product update(Product product) {
        return productRepository.save(product);
    }

    @Override
    public void delete(Product product) {
        productRepository.delete(product);
    }

    @Override
    public List<Product> filter(ProductFilter productFilter) {
        Specification<Product> specification = Specification
                .where(new ProductDescriptionFilter(productFilter.getQ()))
                .or(new ProductNameFilter(productFilter.getQ()))
                .and(new ProductMinPriceFilter(productFilter.getMin_price()))
                .and(new ProductMaxPriceFilter(productFilter.getMax_price()));
        return productRepository.findAll(specification);

    }
}
