package com.uol.desafiojavaspringboot.service;


import com.uol.desafiojavaspringboot.model.Product;
import com.uol.desafiojavaspringboot.model.ProductFilter;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ProductService {
    Product create(Product product);

    List<Product> findAll();



    Optional<Product> findByUuid(UUID id);

    Product update(Product product);

    void delete(Product product);

    List<Product> filter(ProductFilter productFilter);
}
