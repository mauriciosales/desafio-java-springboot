package com.uol.desafiojavaspringboot.resource;

import com.uol.desafiojavaspringboot.model.Product;

import com.uol.desafiojavaspringboot.model.ProductFilter;
import com.uol.desafiojavaspringboot.resource.dto.ProductDTO;
import com.uol.desafiojavaspringboot.resource.dto.ProductFilterDTO;
import com.uol.desafiojavaspringboot.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController()
@RequiredArgsConstructor
@RequestMapping("/products")
@Tag(name = "Product-ms")
public class ProductController {

    private final ModelMapper modelMapper;

    private final ProductService productService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary = "Creating a product")
    public ProductDTO createProduct(@RequestBody @Valid ProductDTO productDTO){
        Product product= modelMapper.map(productDTO,Product.class);

        product=productService.create(product);

        return modelMapper.map(product,ProductDTO.class);


    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Product list")
    public List<ProductDTO> listProduct(){
        List<Product> productList=productService.findAll();

        return productList.stream()
                .map(Entity-> modelMapper.map(Entity, ProductDTO.class))
                .collect(Collectors.toList());
    }


    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Searching for a product by ID")
    public ProductDTO returnProductById( @PathVariable UUID id){


        return productService.findByUuid(id)
                .map(Entity-> modelMapper.map(Entity,ProductDTO.class))
                .orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND));

    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Updating a product")
    public ProductDTO updateProduct(@PathVariable UUID id, @RequestBody @Valid ProductDTO productDTO){



        return productService.findByUuid(id).map(
                product-> {

                    product.setName(productDTO.getName());
                    product.setPrice(productDTO.getPrice());
                    product.setDescription(productDTO.getDescription());

                    Product updatableProduct=productService.update(product);

                    return modelMapper.map(updatableProduct,ProductDTO.class);
                } )
                .orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND));

    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Operation(summary = "Deletion of a product")
    public void deleteProduct(@PathVariable String id){
        if(id==null){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        Product product=productService.findByUuid(UUID.fromString(id)).orElseThrow(()->new ResponseStatusException(HttpStatus.NOT_FOUND));
        productService.delete(product);

    }


    @GetMapping("/search")
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "List of filtered products")
    public List<ProductDTO> searchProductFilter(
                                                @RequestParam(required = false) BigDecimal min_price,
                                                @RequestParam(required = false) BigDecimal max_price,
                                                @RequestParam(required = false) String q){

        ProductFilter productFilter= ProductFilter.builder().q(q).max_price(max_price).min_price(min_price).build();

        return productService.filter(productFilter)
                .stream()
                .map(Entity->modelMapper.map(Entity,ProductDTO.class))
                .collect(Collectors.toList());
    }
}
