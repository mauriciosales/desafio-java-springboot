package com.uol.desafiojavaspringboot.resource.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor

public class ProductDTO {

        private String id;

        @NotNull
        @NotEmpty
        private String description;
        @NotNull
        @NotEmpty
        private String name;
        @NotNull
        @DecimalMin(value = "0.0", inclusive = false)
        @Digits(integer=9, fraction=2)
        private BigDecimal price;

    }
