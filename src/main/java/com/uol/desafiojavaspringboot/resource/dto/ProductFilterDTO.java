package com.uol.desafiojavaspringboot.resource.dto;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class ProductFilterDTO {

    private String q;

    private BigDecimal min_price;

    private  BigDecimal max_price;
}
