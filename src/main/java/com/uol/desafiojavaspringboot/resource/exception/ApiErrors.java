package com.uol.desafiojavaspringboot.resource.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Data
@Getter
@Setter
@AllArgsConstructor

public class ApiErrors {

    private int status_code;

    private String message;

}
