package com.uol.desafiojavaspringboot.model.repository;

import com.uol.desafiojavaspringboot.model.Product;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;

@AllArgsConstructor
public class ProductMinPriceFilter implements Specification<Product> {
    private BigDecimal min_price;

    @Override
    public Predicate toPredicate(Root<Product> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        if(min_price!= null){
            return criteriaBuilder.greaterThanOrEqualTo(root.<String>get("price"), this.min_price.toString());
        }
        return criteriaBuilder.isTrue(criteriaBuilder.literal(true));
    }
}
