package com.uol.desafiojavaspringboot.model.repository;

import com.uol.desafiojavaspringboot.model.Product;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;

@AllArgsConstructor
public class ProductMaxPriceFilter implements Specification<Product> {
    private BigDecimal max_price;

    @Override
    public Predicate toPredicate(Root<Product> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {

        if (max_price != null) {
            return criteriaBuilder.lessThanOrEqualTo(root.<String>get("price"), this.max_price.toString());
        }
        return criteriaBuilder.isTrue(criteriaBuilder.literal(true));
    }
}
