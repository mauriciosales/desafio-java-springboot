package com.uol.desafiojavaspringboot.model.repository;

import com.uol.desafiojavaspringboot.model.Product;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

@AllArgsConstructor
public class ProductDescriptionFilter implements Specification<Product> {

    private String q;

    @Override
    public Predicate toPredicate(Root<Product> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {

        if(q !=null){
            return criteriaBuilder.like(root.<String>get("description"), "%" + this.q + "%");
        }
           return criteriaBuilder.isTrue(criteriaBuilder.literal(true));
    }


}
