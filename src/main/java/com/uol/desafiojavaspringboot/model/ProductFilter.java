package com.uol.desafiojavaspringboot.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@Builder
public class ProductFilter {

    private String q;

    private BigDecimal min_price;

    private  BigDecimal max_price;
}
